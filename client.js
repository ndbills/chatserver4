/**
 * Created with JetBrains WebStorm.
 * User: Nathan
 * Date: 2/3/13
 * Time: 11:03 PM
 * To change this template use File | Settings | File Templates.
 */

var net = require('net');
var util = require('util');

// Setup global variables for the host and port
var HOST, PORT;
var input = true;

// Process the input from the command line args and set the host and port variables
if(process.argv[2] != 'undefined' && process.argv[3] != 'undefined'){
    if(process.argv[2].match(/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/)){
        HOST = process.argv[2];
    } else {
        console.log('Your IP does not match');
        return;
    }
    if(process.argv[3].match(/^[9][0][2][0]$/)){
        PORT = process.argv[3];
    } else {
        console.log('Your port number is invalid!')
        return;
    }
} else {
    // If there are no command line args, set defaults:
    HOST = '127.0.0.1';
    PORT = 9020;
}

//console.log('HOST: ' + HOST);
//console.log('PORT: ' + PORT);

var client = new net.Socket();
client.connect(PORT, HOST, function() {
    // If you need to do something immediately upon connection, do it here
});

/*
This might be fun to look into:
    http://nodejs.org/api/readline.html#readline_rl_setprompt_prompt_length
This would be instead of the implementation below:
*/
process.stdin.resume();
process.stdin.setEncoding('utf8');
process.stdin.on('data', function (message){
    //console.log('you entered: ' + message);
    if(message.trim() == 'adios'){
        client.destroy();
        console.log('Disconnected from host. Please come again!');
        // Close the client socket completely
        process.exit();
    }
    client.write(message.trim() + '\r\n');
});

// Add a 'data' event handler for the client socket
// data is what the server sent to this socket
client.on('data', function(data) {
    console.log(data.toString());
});

