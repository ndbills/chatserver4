/**
 * Created with JetBrains WebStorm.
 * User: Nathan
 * Date: 2/2/13
 * Time: 12:14 AM
 * To change this template use File | Settings | File Templates.
 */

var net = require('net');

var HOST = '0.0.0.0';
var PORT = 9020;

var chatLog = [];

// Create a server instance, and chain the listen function to it
// The function passed to net.createServer() becomes the event handler for the 'connection' event
// The sock object the callback function receives UNIQUE for each connection
net.createServer(function(sock) {
    var chatName = 'Anonymous';
    sock.write("Welcome to Nathan's Chat server.");

    // We have a connection - a socket object is assigned to the connection automatically
    console.log('CONNECTED: ' + sock.remoteAddress +':'+ sock.remotePort);

    // Add a 'data' event handler to this instance of socket
    sock.on('data', function(data) {
        // Accept input and trim the data
        var msg = data.toString().trim();

        //console.log('DATA ' + sock.remoteAddress + ': ' + msg);
        // Write the data back to the socket, the client will receive it as data from the server
        //sock.write('You said "' + msg + '"');
        console.log('msg: ', msg);

        if(msg.match('^help$')){
            sock.write('Commands:\r\n\t' +
                        '"help":   -  -  -> Shows a list of accepted commands.\r\n\t' +
                        '"test: [words]" -> Echos [words].\r\n\t' +
                        '"name: [name]"  -> Sets your name in the chat log.\r\n\t' +
                        '"get"  -  -  -  -> Displays the contents of the chat buffer.\r\n\t' +
                        '"push: [stuff]" -> Adds "[name]: [stuff]" to the chat buffer.\r\n\t' +
                        '"getrange [startline] [endline]" -> Displays contents for chat between [startline] and [endline].\r\n\t' +
                        '"adios"   -  -  -> Quits the current connection.\r\n');
        }
        else if(msg.match('^test: ')){
            sock.write(msg.slice(6) + '\r\n');
            console.log('tester: ', msg.slice(6) + '\r\n');
        }
        else if(msg.match('^name: ')){
            chatName = msg.slice(6);
            sock.write('OK\r\n');
        }
        else if(msg.match('^get$')){
            var chat = '';
            if(chatLog.length > 0){
                for(var i = 0; i < chatLog.length; i++){
                    chat += (chatLog[i] + '\r\n');
                }
                sock.write(chat);
            } else {
                sock.write('There is no current chat buffer.')
            }
        }
        else if(msg.match('^push: ')){
            chatLog.push(chatName + ': ' + msg.slice(6));
            sock.write('OK\r\n')
        }
        else if(msg.match('^getrange [1234567890]+ [1234567890]+$')){
            var range = msg.slice(9).split(' ');
            var chat = '';
            for(var i = range[0]; i <= range[1]; i++){
                chat += (chatLog[i] + '\r\n');
            }
            sock.write(chat);
        }
        else if(msg.match('^cls$')){
            chatLog = [];
        }
        else if(msg.match('^adios$')){
            sock.end('Peace out brotha! (or sista...)');
        }
        else {
            sock.write('Invalid command!');
        }
    });

    // Add a 'close' event handler to this instance of socket
    sock.on('close', function(data) {
        console.log('CLOSED: ' + sock.remoteAddress +' '+ sock.remotePort);
    });

}).listen(PORT, HOST);

console.log('Server listening on ' + HOST +':'+ PORT);