#IT347 - Chat Client and Server
For this project, I am using Node.js and net (a NodeJS module).
# To get this project up and running you need to:
* Install Node.js from nodejs.org
* Install download the project files
* Navigate to the directory of the downloaded project
* Run 'npm install' in the terminal (use the node.js command line in windows)
* Run 'node app.js' to launch the Node.js server
* Run 'node client.js' to launch the client (form a separate node command prompt).
* Ta Da!!!